Deface::Override.new(:virtual_path => 'spree/shared/_search',
  :name => 'change_search_text',
  :replace => "erb[loud]:contains('t(:all_departments)')",
  :text => "
    <%= select_tag :taxon,
          options_for_select([[Spree.t('All'), '']] +
                                @taxons.map {|t| [t.name, t.id]},
                                @taxon ? @taxon.id : params[:taxon]), 'aria-label' => 'Taxon' %>
")